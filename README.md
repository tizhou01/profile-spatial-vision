## Running Setup

To run this profile project, first clone this repo to your local disk then go into the root directory and run `npm i && cd client && npm i`. 

After installing all the dependencies:
 * run `npm run start:server` to start the server 
 * run `npm run start:client` to start the client application.

Open [http://localhost:3000](http://localhost:3000) to view the client application in the browser.

## Note

For google API key, I leave it as plain text in html file for demo purpose. It would be better if those kind of private string can be read from system environment.

This project use mongoDB as the database. To get access to the database, please export an environmental variable `MONGO_URL` in your ~/.bash_profile to indicate the URI to your mongoDB.
