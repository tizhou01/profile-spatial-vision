import React from 'react';
import {withRouter} from 'react-router-dom';

function Navbar(props) {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/">Profile</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <a className="nav-item nav-link" href="/">Home <span className="sr-only">(current)</span></a>
                    <a className={"nav-item nav-link " + (props.isLoggedIn ? "" : "d-none")} href={"/profile"}>My
                        Profile</a>
                </div>
            </div>
            <p className={"mr-sm-2 my-2" + (props.isLoggedIn ? "" : "d-none")}>{sessionStorage.getItem("username")}</p>
            <button className={"btn btn-outline-danger my-2 my-sm-0 " + (props.isLoggedIn ? "" : "d-none")}
                    onClick={() => {
                        props.history.push("/");
                        props.onLogout();
                    }}>Log Out
            </button>
        </nav>
    );
}

export default withRouter(Navbar);
