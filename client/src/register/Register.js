import React from 'react';

export default function Register(props) {
    function constructClassName(prop) {
        return prop === undefined ? "form-control" :
            prop ? "form-control is-valid" : "form-control is-invalid";
    }

    const usernameClassName = constructClassName(props.usernameValid);
    const passwordClassName = constructClassName(props.passwordValid);

    return (
        <div className="row">
            <div className="col-8">
            </div>
            <div className="col">
                <form className="mt-5">
                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className={usernameClassName} aria-describedby="emailHelp"
                               value={props.username}
                               placeholder="Name/Email/Phone Number"
                               onChange={(e) => props.onInputChange("username", e)}/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            {props.usernameFeedback}
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className={passwordClassName} placeholder="Password"
                               value={props.password}
                               onChange={(e) => props.onInputChange("password", e)}/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            {props.passwordFeedback}
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Confirm Password</label>
                        <input type="password" className={passwordClassName} placeholder="Confirm Password"
                               value={props.passwordAgain}
                               onChange={(e) => props.onInputChange("passwordAgain", e)}/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            {props.passwordFeedback}
                        </div>
                    </div>
                    <div className="form-group">
                        <small>Already has an account? <a href={"/login"} className="text-primary">login</a></small>
                    </div>
                    <button className="btn btn-primary" onClick={props.onSubmit}>Register</button>
                </form>
            </div>
        </div>
    );
}
