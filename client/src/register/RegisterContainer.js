import React, {Component} from "react";
import Register from "./Register";

const USER_ALREADY_EXIST = "username has been registered";
const USER_NULL = "please enter valid username";
const PASSWORD_NOT_MATCHED = "passwords are not matched";
const PASSWORD_NULL = "please enter valid password";

export default class RegisterContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            passwordAgain: "",
            usernameValid: undefined,
            passwordValid: undefined,
            usernameFeedback: USER_NULL,
            passwordFeedback: PASSWORD_NULL
        };
    }

    onInputChange = (field, e) => {
        this.setState({
            [field]: e.target.value,
            [field + "Valid"]: undefined
        });
    };

    checkUsername = () => {
        const username = this.state.username;
        const validUsername = username !== "";
        if (!validUsername) {
            this.setState({
                usernameValid: false
            });
        }
        return validUsername;
    };

    checkPassword = () => {
        const password = this.state.password;
        const passwordAgain = this.state.passwordAgain;
        const passwordValid = password === passwordAgain && password !== "";
        this.setState({
            passwordFeedback: password !== passwordAgain ? PASSWORD_NOT_MATCHED : password === "" ? PASSWORD_NULL : "",
            passwordValid: passwordValid ? undefined : false,
            password: passwordValid ? password : "",
            passwordAgain: passwordValid ? passwordAgain : "",
        });
        return passwordValid;
    };

    submit = () => {
        fetch('https://localhost:3001/api/register', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
        }).then(res => res.json()).then(res => {
            console.log(res);
            if (!res.success) {
                this.setState({
                    usernameFeedback: USER_ALREADY_EXIST,
                    usernameValid: false,
                    password: "",
                    passwordAgain: "",
                    passwordValid: undefined
                });
            } else {
                this.props.history.push('/login');
            }
        });
    };


    onSubmit = (e) => {
        e.preventDefault();
        if (this.checkUsername() && this.checkPassword()) {
            this.submit();
        }
    };

    render() {
        return (
            <Register onInputChange={this.onInputChange} onSubmit={this.onSubmit} username={this.state.username}
                      password={this.state.password} passwordAgain={this.state.passwordAgain}
                      usernameFeedback={this.state.usernameFeedback} passwordFeedback={this.state.passwordFeedback}
                      usernameValid={this.state.usernameValid} passwordValid={this.state.passwordValid}/>
        );
    }
}
