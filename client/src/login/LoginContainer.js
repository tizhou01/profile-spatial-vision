import React, {Component} from "react";
import Login from "./Login";

const USER_NULL = "please enter valid username";
const USER_DOESNOT_EXIST = "username does not exist, please autentication first";
const USER_PSW_NOT_MATCH = "username and password are not matched";
const PSW_NULL = "please enter password";

export default class LoginContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            usernameValid: undefined,
            passwordValid: undefined,
            usernameFeedback: "",
            passwordFeedback: ""
        };
    }

    onInputChange = (field, e) => {
        this.setState({
            [field]: e.target.value,
            [field + "Valid"]: undefined
        });
    };

    checkUsername = () => {
        const username = this.state.username;
        if (username === "") {
            this.setState({
                usernameFeedback: USER_NULL,
                usernameValid: false
            });
            return false;
        }
        const password = this.state.password;
        if (password === "") {
            this.setState({
                passwordFeedback: PSW_NULL,
                passwordValid: false
            });
            return false;
        }
        return true;
    };

    submit = () => {
        fetch('https://localhost:3001/api/login', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
        }).then(res => res.json()).then(res => {
            console.log(res);
            if (!res.success) {
                switch (res.message) {
                    case "user does not exist":
                        this.setState({
                            usernameFeedback: USER_DOESNOT_EXIST,
                            usernameValid: false,
                            password: "",
                            username: ""
                        });
                        break;
                    case "user psw does not match":
                        this.setState({
                            usernameFeedback: USER_PSW_NOT_MATCH,
                            passwordFeedback: USER_PSW_NOT_MATCH,
                            usernameValid: false,
                            passwordValid: false,
                            password: "",
                            username: "",

                        });
                        break;
                    default:
                        break;
                }
            } else {
                console.log(res.token);
                sessionStorage.setItem("token", res.token);
                sessionStorage.setItem("username", this.state.username);
                this.props.setLoggedIn();
                this.props.history.push('/profile');
            }
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (this.checkUsername()) {
            this.submit();
        }
    };

    render() {
        return (
            <Login usernameValid={this.state.usernameValid} passwordValid={this.state.passwordValid}
                   onInputChange={this.onInputChange} username={this.state.username}
                   password={this.state.password} usernameFeedback={this.state.usernameFeedback}
                   passwordFeedback={this.state.passwordFeedback} onSubmit={this.onSubmit}/>
        );
    }

}
