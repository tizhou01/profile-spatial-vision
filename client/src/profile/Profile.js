import React, {Component} from 'react';
import FormGroup from "./FormGroup";
import Display from "./Display";

export default class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: sessionStorage.getItem("name"),
            email: sessionStorage.getItem("email"),
            location: sessionStorage.getItem("location"),
            dateofBirth: sessionStorage.getItem("dateofBirth"),
            coordinate: {
                lat: Number(sessionStorage.getItem("lat")),
                lng: Number(sessionStorage.getItem("lng"))
            },
            updated: true,
            nameValid: undefined,
            emailValid: undefined,
            dateofBirthValid: undefined,
            locationValid: undefined,
            progress: sessionStorage.getItem("name") === null ? 0 : 100
        };
    }

    handleDateChange = (date) => {
        this.setState({
            dateofBirth: date,
            dateofBirthValid: undefined
        });
    };

    handleNameChange = (e) => {
        this.setState({
            name: e.target.value,
            nameValid: undefined
        });
    };

    handleEmailChange = (e) => {
        this.setState({
            email: e.target.value,
            emailValid: undefined
        });
    };

    handleLocationChange = (position) => {
        this.setState({
            coordinate: position.position,
            location: position.address,
            locaitonValid: undefined
        });
    };

    checkInput = (field) => {
        let isValid = true;
        let value = this.state[field];
        if (value === null || value === "") {
            isValid = false;
            this.setState({
                [field + "Valid"]: false
            });
        } else this.setState({
            [field + "Valid"]: true
        });
        return isValid;
    };

    onSubmit = () => {
        const nameValid = this.checkInput("name");
        const emailValid = this.checkInput("email");
        const locationValid = this.checkInput("location");
        const dateofBirthValid = this.checkInput("dateofBirth");
        let canSubmit = nameValid && emailValid && locationValid && dateofBirthValid;

        if (canSubmit) {
            this.submitProfileData();
            this.setState({
                updated: true,
                nameValid: undefined,
                emailValid: undefined,
                locationValid: undefined,
                dateofBirthValid: undefined
            });
        }
    };

    onEdit = () => {
        this.setState({
            updated: false
        });
    };

    submitProfileData = () => {
        fetch('https://localhost:3001/api/profile', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': sessionStorage.getItem("token")
                },
                body: JSON.stringify({
                    name: this.state.name,
                    email: this.state.email,
                    location: this.state.location,
                    dateofBirth: this.state.dateofBirth,
                    lat: this.state.coordinate.lat,
                    lng: this.state.coordinate.lng
                })
            }
        ).then(res => res.json()).then(res => {
            if (!res.success) {
                this.props.history.push('/login');
                alert("session timeout! Please login again");
                this.props.onLogout();
            } else {
                sessionStorage.setItem("name", this.state.name);
                sessionStorage.setItem("email", this.state.email);
                sessionStorage.setItem("location", this.state.location);
                sessionStorage.setItem("dateofBirth", this.state.dateofBirth);
                sessionStorage.setItem("lat", this.state.lat);
                sessionStorage.setItem("lng", this.state.lng);
            }
        });
    };

    progressStart = () => {
        this.animation = setInterval(() => {
            this.setState((state) => ({
                progress: state.progress === 100 ? 100 : state.progress + 20
            }));
        }, 100);
    };

    retrieveProfileData = () => {
        this.progressStart();
        fetch(`https://localhost:3001/api/profile?username=${sessionStorage.getItem("username")}`, {
            headers: {
                'x-access-token': sessionStorage.getItem("token")
            }
        })
            .then(res => res.json()).then(res => {
            if (!res.success) {
                if (res.message === "profile not set") {
                    this.setState({
                        updated: false
                    });
                } else if (res.message === "failed to authenticate token") {
                    this.props.history.push('/login');
                    alert("session timeout! Please login again");
                    this.props.onLogout();
                }
            } else {
                clearInterval(this.animation);
                this.setState({
                    progress: 100,
                    name: res.data.name,
                    email: res.data.email,
                    location: res.data.location,
                    dateofBirth: res.data.dateofBirth,
                    coordinate: {
                        lat: res.data.lat,
                        lng: res.data.lng
                    },
                    updated: true
                });
                sessionStorage.setItem("name", res.data.name);
                sessionStorage.setItem("email", res.data.email);
                sessionStorage.setItem("location", res.data.location);
                sessionStorage.setItem("dateofBirth", res.data.dateofBirth);
                sessionStorage.setItem("lat", res.data.lat);
                sessionStorage.setItem("lng", res.data.lng);
            }
        });
    };

    componentDidMount() {
        if (sessionStorage.getItem("token") === null) {
            this.props.history.push("/");
        }
        if (this.state.name === null) {
            this.retrieveProfileData();
        }

        if (this.state.coordinate.lat === 0 && this.state.coordinate.lng === 0) {
            navigator && navigator.geolocation.getCurrentPosition(position => {
                const {latitude, longitude} = position.coords;
                this.setState({
                    coordinate: {
                        lat: latitude,
                        lng: longitude
                    }
                });
            });
        }
    }

    render() {
        return (
            <div>
                <div className={"progress" + (this.state.progress === 100 ? " d-none" : "")} style={{height: "2px"}}>
                    <div className="progress-bar" role="progressbar" aria-valuenow=""
                         style={{width: this.state.progress.toString() + '%'}}
                         aria-valuemin="0"
                         aria-valuemax="100"/>
                </div>
                <div className="card mt-5">
                    <div className="card-body">
                        {!this.state.updated ?
                            <FormGroup handleDateChange={this.handleDateChange}
                                       handleEmailChange={this.handleEmailChange}
                                       handleNameChange={this.handleNameChange}
                                       handleLocationChange={this.handleLocationChange} name={this.state.name}
                                       email={this.state.email} dateofBirth={this.state.dateofBirth}
                                       coordinate={this.state.coordinate} location={this.state.location}
                                       onSubmit={this.onSubmit} nameValid={this.state.nameValid}
                                       emailValid={this.state.emailValid}
                                       locationValid={this.state.locationValid}
                                       dateofBirthValid={this.state.dateofBirthValid}
                            />
                            :
                            <Display name={this.state.name} email={this.state.email} location={this.state.location}
                                     dateofBirth={new Date(this.state.dateofBirth).toLocaleDateString("en-US")}
                                     onEdit={this.onEdit}/>
                        }
                    </div>
                </div>
            </div>

        );
    }
}
