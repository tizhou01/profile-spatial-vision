import DatePicker from "react-datepicker";
import React from "react";
import "react-datepicker/dist/react-datepicker.css";
import LocationPicker from 'react-location-picker';

export default function FormGroup(props) {

    function constructClassName(prop) {
        return prop === undefined ? "form-control" :
            prop ? "form-control is-valid" : "form-control is-invalid";
    }

    let nameClassName = constructClassName(props.nameValid);
    let emailClassName = constructClassName(props.emailValid);
    let locationClassName = constructClassName(props.locationValid);
    let dateofBirthClassName = constructClassName(props.dateofBirthValid);

    return (
        <div>
            <div className="form-row">
                <div className="col-md">
                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input type="text" className={nameClassName} id="name" value={props.name ? props.name : ""}
                               onChange={props.handleNameChange} placeholder="Full name"/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            Please enter your name.
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email"
                               className={emailClassName} id="email" value={props.email ? props.email : ""}
                               onChange={props.handleEmailChange} placeholder="example@domain.com"/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            Please enter valid email address.
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="dateofBirth">Date of Birth</label><br/>
                        <input type="text" className={dateofBirthClassName + " d-none"}
                               id="dateofBirth"/>
                        <DatePicker id="picker" onChange={props.handleDateChange} dropdownMode="select"
                                    className={dateofBirthClassName}
                                    selected={props.dateofBirth ? props.dateofBirth : undefined}
                                    placeholderText="MM/DD/YYYY"/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            Please select your date of birth.
                        </div>
                    </div>
                </div>
                <div className="col-md">
                    <div className="form-group">
                        <label>Location</label><br/>
                        <textarea className={locationClassName} id="location"
                                  value={props.location ? props.location : ""}
                                  readOnly placeholder="select your location in the map"/>
                        <div className="valid-feedback">
                            Looks good!
                        </div>
                        <div className="invalid-feedback">
                            Please select your location.
                        </div>
                        <br/>
                        <LocationPicker containerElement={<div style={{height: '400px'}}/>}
                                        mapElement={<div style={{height: '400px'}}/>} defaultPosition={props.coordinate}
                                        onChange={props.handleLocationChange}/>
                    </div>
                </div>
            </div>
            <button className="btn btn-primary btn-block" onClick={props.onSubmit}>Submit</button>
        </div>
    )
}
