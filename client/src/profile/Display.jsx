import React from "react";

export default function Display(props) {
    return (
        <div>
            <div className="form-row">
                <div className="form-group col-md">
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control readonly-field" id="name" value={props.name}
                           readOnly/>
                </div>
                <div className="form-group col-md">
                    <label htmlFor="email">Email</label>
                    <input type="email" className="form-control readonly-field" id="email" value={props.email}
                           readOnly/>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-md">
                    <label htmlFor="dateofBirth">Date of Birth</label>
                    <input type="dateofBirth" className="form-control readonly-field" value={props.dateofBirth}
                           readOnly/>
                </div>
                <div className="form-group col-md">
                    <label htmlFor="location">Location</label>
                    <textarea className="form-control readonly-field" value={props.location}
                              readOnly/>
                </div>
            </div>
            <button className="btn btn-primary btn-block" onClick={props.onEdit}>Edit</button>
        </div>
    )
}
