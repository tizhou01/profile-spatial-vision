import React, {Component} from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import Profile from "./profile/Profile";
import WelcomeContainer from "./welcome/WelcomeContainer";
import RegisterContainer from "./register/RegisterContainer";
import LoginContainer from "./login/LoginContainer";
import Navbar from "./navbar/Navbar";

export default class AppRouter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: sessionStorage.getItem("token") !== null
        };
    }

    setLoggedIn = () => {
        this.setState({
            isLoggedIn: !this.state.isLoggedIn
        });
    };

    onLogout = () => {
        sessionStorage.clear();
        this.setLoggedIn();
    };

    render() {
        return (
            <Router>
                <Navbar isLoggedIn={this.state.isLoggedIn} onLogout={this.onLogout}/>
                <div className="container">
                    <Route path="/" exact
                           component={(props) => <WelcomeContainer {...props} isLoggedIn={this.state.isLoggedIn}/>}/>
                    <Route path="/login" exact
                           component={(props) => <LoginContainer {...props} setLoggedIn={this.setLoggedIn}/>}/>
                    <Route path="/register" exact
                           component={RegisterContainer}/>
                    <Route path="/profile" component={(props) => <Profile {...props} onLogout={this.onLogout}/>}/>
                </div>
            </Router>
        );
    }
}
