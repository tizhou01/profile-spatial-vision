import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import * as serviceWorker from './serviceWorker';
import AppRouter from "./AppRouter";

ReactDOM.render(<AppRouter/>, document.getElementById('root'));

serviceWorker.register();
