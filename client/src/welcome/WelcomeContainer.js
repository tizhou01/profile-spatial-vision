import React, {Component} from 'react';
import Welcome from "./Welcome";

export default class WelcomeContainer extends Component {
    onLogin = () => {
        this.props.history.push('/login');
    };

    onRegister = () => {
        this.props.history.push('/register');
    };

    render() {
        return (
            <Welcome isLoggedIn={this.props.isLoggedIn} onLogin={this.onLogin} onRegister={this.onRegister}/>
        );
    }
}
