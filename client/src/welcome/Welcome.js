import React from "react";

export default function Welcome(props) {
    return (
        <div className="container mt-5">
            <div className="jumbotron">
                <h1 className="display-4 text-center">{"Welcome to profile! " + (props.isLoggedIn ? sessionStorage.getItem("username") : "")}</h1>
                <br/>
                {props.isLoggedIn ? <p className="text-center">you car start viewing and editing your profile</p> :
                    <p className="text-center">please login or register</p>}
                <br/>
                <div className="row">
                    {props.isLoggedIn ?
                        <div className="mx-auto">
                            <a className="btn btn-primary btn-lg m-1 text-white" href={"/profile"}>View Your Profile</a>
                        </div>
                        :
                        <div className="mx-auto">
                            <button className="btn btn-primary btn-lg m-1" onClick={props.onLogin}>Login</button>
                            <button className="btn btn-success btn-lg m-1" onClick={props.onRegister}>Register</button>
                        </div>}

                </div>
            </div>
        </div>
    );
}
