import Profile from '../models/profile';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import {getSecret} from '../secrets';

export const register = async (req, res) => {
    const {username, password} = req.body;
    const hashedPassword = bcrypt.hashSync(password, 8);
    const result = await Profile.find({username: username});
    if (result.length > 0) {
        return res.json({
            success: false, message: "user already exist"
        });
    }
    const profile = new Profile();
    profile.username = username;
    profile.password = hashedPassword;
    profile.save((err) => {
        if (err) return res.json({success: false, error: error});
        return res.json({success: true});
    });

};

export const login = async (req, res) => {
    const {username, password} = req.body;
    const user = await Profile.findOne({username: username});
    if (!user) {
        return res.json({
            success: false, message: "user does not exist"
        });
    }
    let passwordIsValid = bcrypt.compareSync(password, user.password);
    if (!passwordIsValid) return res.json({
        success: false, message: "user psw does not match"
    });
    let token = jwt.sign({id: user._id}, getSecret("jwtSecret"), {
        expiresIn: 600 // expires in 10 mins
    });
    return res.json({success: true, token: token});
};
