import * as jwt from 'jsonwebtoken';
import {getSecret} from '../secrets';

export const verifyToken = (req, res, next) => {
    const token = req.headers['x-access-token'];
    if (!token) return res.json({
        success: false,
        message: "no token provided"
    });
    jwt.verify(token, getSecret("jwtSecret"), (err, decode) => {
        if (err) return res.json({success: false, message: "failed to authenticate token"});
        req.userId = decode["id"];
        next();
    });
};
