import Profile from '../models/profile';

export const getProfile = async (req, res) => {
    const profile = await Profile.findById(req["userId"]);
    if (!profile.location) {
        return res.json({
            success: false,
            message: "profile not set"
        });
    } else {
        let response = {};
        response.success = true;
        response.data = profile;
        return res.json(response);
    }
};


export const putProfile = async (req, res) => {
    const {name, email, location, dateofBirth, lat, lng} = req.body;
    const profile = await Profile.findById(req["userId"]);
    profile.name = name;
    profile.email = email;
    profile.location = location;
    profile.dateofBirth = dateofBirth;
    profile.lat = lat;
    profile.lng = lng;
    profile.save(err => {
        if (err) return res.json({success: false, message: error});
        return res.json({success: true});
    });
};
