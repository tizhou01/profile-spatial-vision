import mongoose from 'mongoose';
import {getSecret} from './secrets';

mongoose.connect(getSecret('dbUri'), {useNewUrlParser: true}).catch();

const db = mongoose.connection;

export default db;
