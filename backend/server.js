import express from 'express';
import https from 'https';
import path from 'path';
import db from './db';
import fs from 'fs';
import bodyParser from 'body-parser';
import cors from 'cors';
import {getProfile, putProfile} from "./controllers/profile";
import {register, login} from "./controllers/authentication";
import {verifyToken} from "./controllers/middleware";

// for local env ssl
const certOptions = {
    key: fs.readFileSync(path.resolve('build/cert/server.key')),
    cert: fs.readFileSync(path.resolve('build/cert/server.crt'))
};

const app = express();

const PORT = 3001;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

db.on('error', console.error.bind(console, 'MongoDB connection error'));

app.post('/api/register', register);
app.post('/api/login', login);
app.get('/api/profile', verifyToken, getProfile);
app.put('/api/profile', verifyToken, putProfile);

https.createServer(certOptions, app).listen(PORT);

