// model/profile.js
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create new instance of the mongoose.schema. the schema takes an
// object that shows the shape of your database entries.
const ProfilesSchema = new Schema({
    username: String,
    password: String,
    name: String,
    email: String,
    dateofBirth: String,
    location: String,
    lat: Number,
    lng: Number
}, {timestamps: true});


// export our module to use in server.js
export default mongoose.model('profile', ProfilesSchema);
