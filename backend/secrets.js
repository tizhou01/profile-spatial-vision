const secrets = {
    dbUri: process.env.MONGO_URL,
    jwtSecret: process.env.JWT_SECRET
};

export const getSecret = key => secrets[key];
